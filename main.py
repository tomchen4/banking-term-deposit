import pandas as pd
import numpy as np
from sklearn import preprocessing
import matplotlib.pyplot as plt 
plt.rc("font", size=14)
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import seaborn as sns
import os
import sys
from DataExploration import exploreAndCleanData, balanceData, featureElimination
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

detail = False;
if len(sys.argv) > 1:
	detail = (sys.argv[1] == "--detail") or (sys.argv[1] == "-d")

## Clean data (dummies, scale, and removal of empty cells)
unbalancedData = exploreAndCleanData('banking.csv', detail)

## divide to train and test
X = unbalancedData.loc[:, unbalancedData.columns != 'y']
y = unbalancedData.loc[:, unbalancedData.columns == 'y']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

## balance the data, and eliminate unwanted features (when we're finished, we have 19 features)
X, y = balanceData(X_train, y_train)
#X = featureElimination(X, y);

# # remove the unwanted features from test set. 
# for i in X_test.columns:
# 	if i not in X.columns:
# 		del(X_test[i])

# implement logistic regression
logreg = LogisticRegression(solver = 'lbfgs')
logreg.fit(X, y.values.ravel())
predictedY = logreg.predict(X_test)
print('Accuracy of logistic regression classifier on test set: ', logreg.score(X_test, y_test))

confusionMatrix = confusion_matrix(y_test, predictedY)
print(confusionMatrix)

print(classification_report(y_test,predictedY))

## ROC curve
logit_roc_auc = roc_auc_score(y_test, logreg.predict(X_test))
fpr, tpr, thresholds = roc_curve(y_test, logreg.predict_proba(X_test)[:,1])
plt.figure()
plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.savefig('graphs/Log_ROC')
plt.show()