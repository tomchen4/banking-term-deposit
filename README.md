# Stages:

## Data Exploration:

1. Read data, and remove rows with missing values (dropna)
2. Read the data, and if there is a field with too much irrelevent categories, group the ones you can. (in our case, education field has 3 "basic" categories)
3. print and plot the distribution of y
4. if the table is imbalanced, we need to balance it. (after finishing exploration)
5. grouby y (data.groupby('y').mean()), and look for linear dependency of features. (if there is high correlation between features, we'll need to remove them) in this case:
	* we see subscribers are, by average, a year older then non-subscribers.
	* suprisingly, campaigns field (num. of contacts to the customer in the last campaign) is, in average, higer for the unsubscribers.
	* pdays (how many days since last contacted) i5♣s significaly lower for the subscribers (reasonable)
	* duration field, is higly different- yet irrelevent, since the call duration is known only after the call ended (and after we know y)- this field need to go.
	* previous, number of calls before the campaign, is almost two times bigger for subscribers.
	* emp.var.rate, cons.price.idx, cons.conf.idx, euribor3m, nr.employed are general and periodically changed parameters (social and economics parmas), hence we'll not deduct conclusions about them.

6. continue grouping by fields (mostly categorical fields), for better sense of the data.
	* students are the job with subscribers- 31% of students sunscribed (avg). 25.2% of retired clients subscribed as well. this goes to as low as 6.8% for blue-collar.
	* over 22% of the illiterates subscribed. this lowers extremly to 8% when having basic education. 10-13% on other know educational statuses.
	* only 18 illiterates are present in the dataset, therefore the above statement is problematic.
	* around 50% of the client that was last contacted in march and december subscribed. this goes as low as 6.4% in march.

7. Now, we explore by visualization
	* Marital status is not higly correlated to purchasing.
	* As we saw, job seem like a good predicator, and also education.
	* the day of the week seem like a weak predicator.
	* Month seem like a pretty good indicator.
	* Most of the banks clients are 30-60, majority between 30-40
	* Poutcome seems to be a good predictor of the outcome variable.

## Data Manipulations:

1. First, we want to use dummies to get rid of our categorical data- we'll make every category a binary feature.
2. Next, we want to scale the data
3. divide the data into training and test.
4. next, we have to balance our test-data. we'll use oversampling (SMOTE) or undersampling (for example, randomly deleting majority samples)
	* *Important!* balance only the training data.
5. Dimensionality reduction- we can use RFE to eliminiate unrelated features, or use PCA to reduce the dimension
6. Statistical summary- we used logit function in statsmodels.formula.api, and got a summary of the data. if any of the columns has high p value (>0.05), we should remove them.

## Model Implementation && Evaluation

1. First, we have to fit the model- we can use sklearns models (in our case, sklearn.linear_model.LogisticRegression)
2. then, we make a prediction on the test set, and see our accuracy (score)
3. Continue to evaluate the confusion matrix, percision, recall & F-score.
4. plot the ROC Curve.





Tom Chen, 01/11/2019