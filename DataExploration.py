import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
plt.rc("font", size=14)
import seaborn as sns
from sklearn.feature_selection import RFE
from imblearn.over_sampling import SMOTE
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler
import statsmodels.formula.api as sm
import os

def exploreAndCleanData(csvPath, detail = True):
	sns.set(style="white")
	sns.set(style="whitegrid", color_codes=True)

	############### ---------------------Clean data & unite categories--------------------- ###############
	data = pd.read_csv('banking.csv')

	withna = data.shape[0]
	columns = data.columns;
	print("Number of examples: ", withna)
	print("Number of features: ", data.shape[1] - 1)
	pd.set_option('display.max_columns', len(columns))
	data = data.dropna()
	print("deleted", withna - data.shape[0], "rows.")

	print(data.head())
	print("##################################################################")
	print("We have 20 features: \n age (num)\n job (cat)\n marital (cat)\n education (cat)\n default (has credit? cat)\n housing (has house loan? cat)\n loan (has personal loan? cat)")
	print(" contact (cat)\n month\n day_of_week\n duration (num)\n campaign (contacts made w the client in this campaign- num)\n pdays (days since last contacted- num)")
	print(" poutcome (outcome of the last marketing campaig- cat)\n\nand 5 numeric non-client features that are periodically updated: ")
	print(" emp.var.rate- changes in employees (hired/fired)\n cons.price.idx (consumer price index)\n cons.conf.idx (consumer confidence index)\n euribor3m (Euribor 3 months interest rate on Euro coin)\n nr.employees (number of employees)\n")
	print("##################################################################")


	# data['education']=np.where(data['education'] =='basic.9y', 'Basic', data['education'])
	# data['education']=np.where(data['education'] =='basic.6y', 'Basic', data['education'])
	# data['education']=np.where(data['education'] =='basic.4y', 'Basic', data['education'])
	#print(data['education'].unique())

	############### ---------------------Data exploration--------------------- ###############
	if detail:
		## display the distribution of y
		print(data['y'].value_counts())
		print("We can see high imbalance in the dataset")
		sns.countplot(x='y', data=data)
		plt.savefig('graphs/y_dist');
		plt.show()

		num_subscribed = len(data[data["y"] == 1])
		num_unsubscribed = len(data[data["y"] == 0])
		print("precentage of subscribed is ", 100 * num_subscribed / len(data["y"]))
		print("precentage of unsubscribed is ", 100 * num_unsubscribed / len(data["y"]))

		print("Now, we can see that the data is imbalance, so let's keep exploring")
		print("grouping by target to notice any correlations of the average")
		print(data.groupby('y').mean())
		print("We can see that, by average, the features with low difference in relation to the target class, are: (age, cons_price_idx, cons_price_idx, nr_employed), and therefore, they might not be very predictive")
		print("We can see that, by average, the features with high difference in relation to the target class, are: (duration, pdays, previous, emp_var_rate, euribor3m) and therefore, they might be predictive")
		print("Feature campaign is interesting, for the difference is not large, yet the higher contact (during last campaign) average goes to the non-subscribers class.")


		print("Job categories, and the precentage of clients for each")
		print(data['job'].value_counts()*100/withna)
		print("only a 0.8% of the clients has unknown job.")
		print("let's look at the precentage of subscribers")
		print(data.groupby("job").mean()["y"]*100)
		print("there is high difference for y-precentage, seems like a good category")
		print("##################################################################")
		print("\nNow, let's check marital")

		print("Marital categories, and the precentage of clients for each")
		print(data['marital'].value_counts()*100/withna)
		print("only a 0.19% of the clients has unknown marital status- and 60.5% are married. it seems wise to eliminate the unknow category.")
		print("let's look at the precentage of subscribers")
		print(data.groupby("marital").mean()["y"]*100)
		print("divorced and married people has by avg about 10% subscribers. singles (and unknown) are 15%")

		print("So, we decided to remove the unknown category, and add it to the large group (married). it has a minor effect, since it is 0.3% of that group.")
		data["marital"] = data["marital"].replace({"unknown":"married"})
		print(data.groupby("marital").mean()["y"]*100)
		print("We can see there is little difference from our change, so we can continue")

		print("##################################################################")
		print("\nNow, let's check education")
		print("Education categories, and the precentage of clients for each:")
		print(data['education'].value_counts()*100/withna)
		print("only 0.04% of the clients are illiterate- We should consider combining it with basic.4y")
		print(data.groupby("education").mean()["y"]*100)
		print("Let's try merging illiterates into basic.4y")
		data["education"] = data["education"].replace({"illiterate":"basic.4y"})
		print(data.groupby("education").mean()["y"]*100)
		print("Only 0.05% change, so we can continue.")
		

		# education field has a lot of categories (basic.4/6/9y)- lets check if we should reduce them to one.
		print("##################################################################")
		print("\nthere are 3 basic education levels, and we want to see how different they are from each other")
		print("let's start by watching the average value of the class field (subscribers) grouped by education: \n")
		print(data[data["education"].str.find("basic") != -1].groupby('education').mean()['y']*100)
		print("As we can see, there is a difference between the three basic categories by mean")
		print("So now, We want to see how many examples there are for basic category")
		print(data['education'].value_counts())
		print("And now, as a precentage from the total amount of examples")
		print(data['education'].value_counts()*100/withna)
		print("We can see that about 30% of the examples are basic at some level")
		print("5.56% of the examples are basic.6y, yet i decided not to make a group for basic.6y.or.less, since there is 2% difference between 4y and 6y")

		print("##################################################################")
		print("\nNow, let's check month")
		print("subscribers precentage by month")
		print(data.groupby("month").mean()["y"]*100)
		print("There is a lot of difference, on avg, betweeen the last contact month and subscribers precentage, and it might be a good indicatore.")

		print("##################################################################")
		print("\nNow, let's check default (client has credit by default)")
		print(" 'Default' categories, and the precentage of clients for each:")
		print(data['default'].value_counts()*100/withna)
		print("This field is higly imbalanced, with less then 0.01% of 'yes', and almost 21% 'unknown' which makes one think if this is a critical feature")
		print("Let's look at the precentage of subscribers")
		print(data.groupby("default").mean()["y"]*100)
		print("So, there is high 13% subscribers from tho 'no' category, and 0 for the 'Yes'. much lower 5.15% for the 'unknown'. let's try and merge 'Yes' to 'unkown':")
		data["default"] = data["default"].replace({"yes":"unknown"})
		print(data['default'].value_counts()*100/withna)
		print(data.groupby("default").mean()["y"]*100)
		print("since it changes only 0.002%, we will leave the data changed.")
		#os.system("pause")

		print("##################################################################")
		print("\nNow, let's check housing (client has house loans)")
		print(" 'Default' categories, and the precentage of clients for each:")
		print(data['default'].value_counts()*100/withna)


		## now, we'll visualize the data- distribution of subscribers by job.
		pd.crosstab(data.job,data.y).plot(kind='bar')
		plt.title("Purchases distribution by job")
		plt.xlabel('Job')
		plt.ylabel('No. of clients')
		plt.savefig('graphs/purchases-for-jobs')
		plt.show()

		table = pd.crosstab(data.job,data.y)
		# sum the columns, set them to float, and divide the rows by the sum (to get the precentage) 
		table.div(table.sum(1).astype(float), axis=0).mul(100).plot(kind='bar', stacked=True)
		plt.title("Purchases distribution by job")
		plt.xlabel('Job')
		plt.ylabel('precentage of clients')
		plt.savefig('graphs/purchases-for-jobs-precentage-stacked')
		plt.show()


		# distribution of subscribers by marital
		table = pd.crosstab(data.marital, data.y)
		table.div(table.sum(1).astype(float), axis=0).mul(100).plot(kind='bar', stacked=True)
		plt.xticks(rotation=15, ha='left')
		plt.title("Purchases distribution by marital")
		plt.xlabel("Marital")
		plt.ylabel("precentage of clients")
		plt.savefig('graphs/Marital-to-subscribers-precentage-stacked')
		plt.show()

		# distribution of subscribers by education
		table = pd.crosstab(data.education, data.y)
		table.div(table.sum(1).astype(float), axis=0).mul(100).plot(kind='bar', stacked=True)
		#turn the x ticks
		plt.xticks(rotation=15, ha='right')
		plt.title("Purchases distribution by education")
		plt.xlabel("education")
		plt.ylabel("precentage of clients")
		plt.savefig('graphs/education-to-subscribers-precentage-stacked')
		plt.show()

		# distribution of subscribers by day of the week
		table = pd.crosstab(data["day_of_week"], data.y)
		table.div(table.sum(1).astype(float), axis=0).mul(100).plot(kind='bar', stacked=True)
		plt.xticks(rotation=15, ha='right')
		plt.title("Purchases distribution by day")
		plt.xlabel("Day")
		plt.ylabel("precentage of clients")
		plt.savefig('graphs/day-to-subscribers-precentage-stacked')
		plt.show()

		# distribution of subscribers by month
		table = pd.crosstab(data["month"], data.y)
		table.div(table.sum(1).astype(float), axis=0).mul(100).plot(kind='bar', stacked=True)
		plt.xticks(rotation=15, ha='right')
		plt.title("Purchases distribution by month")
		plt.xlabel("Month")
		plt.ylabel("precentage of clients")
		plt.savefig('graphs/month-to-subscribers-precentage-stacked')
		plt.show()

		# histogram of the clients ages
		data.age.hist()
		plt.title("Clients distribution by Age")
		plt.xlabel("Age")
		plt.ylabel("Number of clients")
		plt.savefig('graphs/age-to-clients-histogram')
		plt.show()

		pd.crosstab(data.poutcome, data.y).plot(kind="bar")
		plt.title("Clients distribution by outcome of last call")
		plt.xlabel("Outcome of last call")
		plt.ylabel("Number of clients")
		plt.savefig('graphs/poutcome-to-clients-histogram')
		plt.show()
		#lt.clf()
		
	############### --------------------- Data manipulation--------------------- ###############
	
	##change Categorial variabels to features
	catalogVars = ['job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'month', 'day_of_week', 'poutcome']
	allVars=data.columns.values.tolist()
	for var in catalogVars:
		dummies = pd.get_dummies(data[var], prefix=var)
		data=data.join(dummies)
	dataBeforeScale = data.drop(catalogVars, axis=1)
	columns = dataBeforeScale.columns;

	#### Scale the data
	scaler = MinMaxScaler();
	return pd.DataFrame(scaler.fit_transform(dataBeforeScale), columns=columns);

###################################################################################################################################################

def balanceData(X, y):
	os = SMOTE(random_state=0)
	columns = X.columns
	resempledX, resempledY = os.fit_resample(X, y.values.ravel())
	resempledX = pd.DataFrame(resempledX, columns = columns)
	resempledY = pd.DataFrame(resempledY, columns=['y'])
	m = len(resempledX)
	# Let's see we resempled data correctly
	print("length of oversampled data is ",m)
	print("Number of no subscription in oversampled data",len(resempledY[resempledY['y']==0]))
	print("Number of subscription",len(resempledY[resempledY['y']==1]))
	print("Proportion of no subscription data in oversampled data is ",len(resempledY[resempledY['y']==0])/m)
	print("Proportion of subscription data in oversampled data is ",len(resempledY[resempledY['y']==1])/m)

	return resempledX, resempledY


###################################################################################################################################################

def featureElimination(X, y):
	logreg = LogisticRegression(solver='lbfgs', max_iter=500)
	
	# coose number of features to keep	
	rfe = RFE(logreg, 20)
	rfe.fit(X, y.values.ravel())
	columns = X.columns
	print(X.columns)
	print(len(X.columns))
	print(rfe.support_)
	print(rfe.ranking_)
	for i in range(0, (len(rfe.support_)-1)):
		if not rfe.support_[i]:
			del X[columns[i]]
	print(X.columns)
	print(len(X.columns))

	## For time saving purposes, I hard coded the features selected.

	#selectedColumns = ['duration', 'campaign', 'pdays', 'previous', 'emp_var_rate', 'cons_price_idx', 'euribor3m', 'job_retired', 'job_student','education_illiterate', 'default_unknown', 'contact_telephone', 'month_aug', 'month_dec', 'month_jun', 'month_mar', 'month_may', 'month_nov', 'poutcome_failure', 'poutcome_success']
	# for i in X.columns:
	# 	if i not in selectedColumns:
	# 		del X[i]
	## Statistical testing
	formula = 'y~' + '+'.join(X.columns)
	logit = sm.logit(formula, X)
	result = logit.fit()
	print(result.summary2())

	# We saw the feature education_illiterate has high p-value, so we removed it.
	del X["education_illiterate"]
	formula = 'y~' + '+'.join(X.columns)
	logit = sm.logit(formula, X)
	result = logit.fit()
	print(result.summary2())
	return X;

